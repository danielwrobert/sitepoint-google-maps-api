var map;

//Create a single infowindow
var infoWindow = new google.maps.InfoWindow();// Create the info window


// Function run on DOM load
function loadMap() {
	var mapId = document.getElementById( 'map' );
	var mapOptions = {
		// Map center ( required )
		center: { lat: 39.828127, lng: -98.579404 },

		// Zoom on load ( required )
		zoom: 5,

		styles: shiftWorkerMapStyle
	};

	// Create the map
	map = new google.maps.Map( mapId, mapOptions );

	// Loop through the airport data and create new marker for each airport
	for ( var i = 0, ii = airportData.length; i < ii; i++ ) {
		var airport = airportData[i];

		// Marker creation
		var newMarker = this.addMarker( airport );

		//Avg percentage
		airport.totalper = ( airport.aper + airport.dper ) / 2;

		//Total flights
		airport.totalflights = ( airport.aop + airport.dop );

		//Set the icon size
		if ( airport.totalflights > 10000 ) {
			airport.iconsize = new google.maps.Size( 48, 48 );
		}
		else if ( ( 1000 <= airport.totalflights ) && ( airport.totalflights <= 10000 ) ) {
			airport.iconsize = new google.maps.Size( 32, 32 );
		}
		else if ( airport.totalflights < 1000 ) {
			airport.iconsize = new google.maps.Size( 16, 16 );
		}

		//Set the icon color
		if ( airport.totalper >= 80 ) {
			airport.icon = 'green';
		}
		else if ( ( 70 <= airport.totalper ) && ( airport.totalper < 80 ) ) {
			airport.icon = 'yellow';
		}
		else if ( ( 60 <= airport.totalper ) && ( airport.totalper < 70 ) ) {
			airport.icon = 'orange';
		}
		else {
			airport.icon = 'red';
		}

		//Add the marker to the map
		newMarker = addMarker( airport );

		// Bind Airport data to new marker
		newMarker.airport = airport;

		// Adds info window to marker
		addInfoWindow( newMarker );
	}
}


// Add a marker to the map
function addMarker( airport ) {
	// Create the marker #MarkerOptions
	var marker = new google.maps.Marker( {
		// Position of marker
		position: new google.maps.LatLng( airport.lat, airport.lng ),
		map: map, // Alternatively, you can assign to map with setMap method (see below)
		icon: {
			url: 'img/airplane-' + airport.icon + '.png',
			size: airport.iconsize,
			origin: new google.maps.Point( 0, 0 ),
			anchor: new google.maps.Point( 16, 32 ),
			scaledSize: airport.iconsize
		},
		// Sets title when mouse hovers
		title: airport.airport,
	} );

	return marker;
}


// Associate an info window with a marker
function addInfoWindow( marker ) {
	var details = marker.airport;

	// Content string
	var contentString = '<div class="infowindowcontent">' +
		'<div class="row">' +
		'<p class="total ' + details.icon + 'bk">' + Math.round( details.totalper * 10 ) / 10 + '%</p>' +
		'<p class="location">' + details.airport.split( "(" )[0].substring( 0, 19 ) + '</p>' +
		'<p class="code">' + details.code + '</p>' +
		'</div>' +
		'<div class="data">' +
		'<p class="tagbelow">Avg On-Time</p>' +
		'<p class="label">Arrivals</p>' +
		'<p class="details">' + details.aper + '% (' + numberWithCommas( details.aop ) + ')</p>' +
		'<p class="label">Departures</p>' +
		'<p class="details">' + details.dper + '% (' + numberWithCommas( details.dop ) + ')</p>' +
		'<p class="coords">' + details.lat + ' , ' + details.lng + '</p>' +
		'</div>' +
		'</div>';

	//Add click event listener
	google.maps.event.addListener( marker, 'click', function( e ) {
		// Close any open infowindows
		infoWindow.close();

		// Set the new content
		infoWindow.setContent( contentString );

		// Open the infowindow on click
		infoWindow.open( map, marker );
	} );
}

//Add Commas to number
function numberWithCommas( x ) {
	return x.toString().replace( /\B(?=(\d{3})+(?!\d))/g, ',' );
}

google.maps.event.addDomListener( window, 'load', loadMap );
