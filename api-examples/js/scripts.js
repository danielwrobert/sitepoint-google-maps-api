var map;

//Get the location to display the coordinates
var lat = document.getElementById( 'latcoords' );
var lng = document.getElementById( 'loncoords' );

// Function run on DOM load
function loadMap() {
	var mapId = document.getElementById( 'map' );
	var mapOptions = {
		// Map center ( required )
		//center: { lat: 34.8493453, lng: -82.3957583 },
		center: { lat: 40.748817, lng: -73.985428 },

		// Zoom on load ( required )
		//zoom: 14,
		zoom: 11,

		// Limit min/max zoom
		//minZoom: 10,
		//maxZoom: 12,

		// Map control
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
			mapTypeIds: [google.maps.MapTypeId.ROADMAP,
						google.maps.MapTypeId.SATELLITE,
						google.maps.MapTypeId.HYBRID,
						google.maps.MapTypeId.TERRAIN],
			position: google.maps.ControlPosition.TOP_RIGHT
		},

		// Set maptype
		mapTypeId: google.maps.MapTypeId.TERRAIN,

		// 0 to 45deg, only valid for satellite and terrain and zoomed in
		//tilt: 0,

		// Zoom controls
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.MapTypeControlStyle.SMALL, // Default
			position: google.maps.ControlPosition.RIGHT_TOP
		},

		// Pan controls ( Removed from latest version )
		//panControl: true,
		//panControlOptions: {
			//position: google.maps.ControlPosition.TOP_LEFT
		//},

		// Street view control
		streetViewControl: true, // default
		streetViewControlOptions: {
			position: google.maps.ControlPosition.RIGHT_TOP
		},

		// Overview map ( Removed from latest version )
		//overviewMapControl: true,
		//overviewMapControlOptions: {
			//opened: true
		//}

		// Set the map style
		//styles: customMapStyle
		//styles: cladmeMapStyle
		styles: shiftWorkerMapStyle
	};

	// Create the map
	map = new google.maps.Map( mapId, mapOptions );

	updateCurrentLatLng( map.getCenter() );

	// Update the URL with the current location
	//updateUrlLocation( map.getCenter(), map.getZoom() );

	mapEventListeners();

	// Marker creation
	var newMarker = this.addMarker();

	// Adds info window to marker
	addInfoWindow( newMarker );

	// Trigger marker info window
	google.maps.event.trigger( newMarker, 'click' );
}

// Add the map event listeners
function mapEventListeners() {
	// Mouse move updates the coordinates
	var mouseMoveChanged = google.maps.event.addListener( map, 'mousemove', function( e ) {
		// Update the coordinates
		updateCurrentLatLng( e.latLng );
	} );

	var mouseDoubleCLick = google.maps.event.addListener( map, 'rightclick', function( e ) {
		// Get the map zoom and increment
		var z = map.getZoom() + 1;

		// Increment the zoom or reset
		if ( z < 16 ) {
			map.setZoom( z );
		} else {
			map.setZoom( 11 );
		}

		// Set the map center to the mouse click
		map.setCenter( e.latLng );
	});

	// Wait for map to load
	var listenerIdle = google.maps.event.addListenerOnce( map, 'idle', function() {
		console.log( 'Map has loaded!' );
	} );

	// Drag End
	var listenerDragEnd = google.maps.event.addListener( map, 'dragend', function() {
		//updateUrlLocation( map.getCenter(), map.getZoom() );
		console.log( 'Drag End event fired' );
	} );

	// Zoom changed
	var listenerZoomChanged = google.maps.event.addListener( map, 'zoom_changed', function() {
		//updateUrlLocation( map.getCenter(), map.getZoom() );
		console.log( 'Zoom Chaanged event fired' );
	} );

}

// Update the position of the mouse in latitude and longitude
function updateCurrentLatLng( latLng ) {
	// Update the coordinates
	lat.innerHTML = latLng.lat();
	lng.innerHTML = latLng.lng();
}

// Update the URL with the map senter and zoom
function updateUrlLocation( center, zoom ) {
	var state = { 'center': center, 'zoom': zoom };
	var title = 'Map Center';
	var url = '?lat=' + center.lat() + '&lon=' + center.lng() + '&zoom=' + zoom;

	// Set the URL
	window.history.pushState( state, title, url );
	// This seems to be causing an error. Could be because server is not running?
	// If that is not the issue, test with window.location instead of history.pushState.
}

// Add a marker to the map
function addMarker() {
	// Create the marker #MarkerOptions
	var marker = new google.maps.Marker( {
		// Position of marker
		position: new google.maps.LatLng( 40.6413111, -73.77813909 ),
		map: map, // Alternatively, you can assign to map with setMap method (see below)
		icon: {
			url: 'img/airplane-green.png',
			size: new google.maps.Size( 32, 32 ),
			origin: new google.maps.Point( 0, 0 ),
			anchor: new google.maps.Point( 16, 32 ),
			scaledSize: new google.maps.Size( 32, 32 )
		},
		// Set the animation (BOUNCE or DROP -- Defaule = none)
		animation: google.maps.Animation.DROP,
		// Sets whether marker is clickable (Default = false)
		clickable: true,
		// Drag marker (Default - false)
		draggable: true,
		// Control cross under draggable marker (Default = true)
		crossOnDrag: false,
		// Set marker opacity (0-1)
		opacity: 1.0,
		// Sets title when mouse hovers
		title: 'New York, NY (JFK)',
		// Controls marker visibility (true or false -- Default = true)
		visibiity: true,
		// Controls zIndex of multiple markers displayed in close proximity (Default is based on vertical position on map)
		zIndex: 1
	} );
	// Note: Can use get and set methods to control marker properties after marker creation

	//marker.setMap( map );
	//marker.setVisible( false ); // Can toggle visibility based on event, for example (see API docs for additional methods).

	return marker;
}

// Associate an info window with a marker
function addInfoWindow( marker ) {

	// Content string 
	var contentString = '<div class="infowindowcontent">'+
		'<div class="row">' +
		'<p class="total greenbk">78.3%</p>'+
		'<p class="location">NEW YORK NY</p>'+
		'<p class="code">JFK</p>'+
		'</div>'+
		'<div class="data">'+
		'<p class="tagbelow">Avg On-Time</p>'+
		'<p class="label">Arrivals</p>'+
		'<p class="details">76% (8,590)</p>' +
		'<p class="label">Departures</p>'+
		'<p class="details">80.5% (8,589)</p>' +
		'<p class="coords">40.6413111 , -73.77813909</p>' +
		'</div>' +
		'</div>';

	// Create the info window
	var infoWindow = new google.maps.InfoWindow( {
		//Set the content of the infowindow
		content: contentString,
		//Pan the map if infowindow extends offscreen
		disableAutoPan: false,
		//Set the max width
		maxWidth: 300,
		//Set the zIndex when overlaying
		zIndex: 1
	} );

	//Add click event listener
	google.maps.event.addListener( marker, 'click', function( e ) {
		//Open the infowindow on click
		infoWindow.open( map, marker );
	} );
}

google.maps.event.addDomListener( window, 'load', loadMap );
