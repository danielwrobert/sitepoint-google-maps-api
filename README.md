# SitePoint Google Maps API

SitePoint course "[Google Maps API](https://www.sitepoint.com/premium/courses/introduction-to-google-maps-api-2895)" example code.

Course repository can be found [here](https://github.com/learnable-content/introduction-to-google-maps-api).